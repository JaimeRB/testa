*   Mi primera página con estilo[Página principal](indice.html)
*   [Meditaciones](meditaciones.html)
*   [Mi ciudad](ciudad.html)
*   [Enlaces](enlaces.html)

Mi primera página con estilo
============================

¡Bienvenido a mi primera página con estilo!

No tiene imágenes, pero tiene estilo. También tiene enlaces, aunque no te lleven a ningún sitio…

Debería haber más cosas aquí, pero todavía no sé qué poner.

Creada el 5 de abril de 2004

por mí mismo.